import java.util.*;

public class Main {
    public static void main(String[] args) {
        String [][] array2D = {{"kazuya", "jin", "lee"}, {"kazuya", "feng"} };
        List<List<String>> words = convertArrayToList(array2D);
        System.out.println(words);

        Set<String> arrays = new LinkedHashSet<String>();
        for (int i = 0; i<words.size(); i++) {
            for (int j = 0; j<words.get(i).size(); j++) {
                arrays.add(words.get(i).get(j));
            }
        }

        System.out.println(arrays);
    }

    public static List<List<String>> convertArrayToList(String[][] arrays) {
        List<List<String>> arrayList2D = new ArrayList<List<String>>();
        for (int i = 0; i<arrays.length; i++) {
            List<String> eachRecord = new ArrayList<String>();
            for (int j = 0; j<arrays[i].length; j++) {
                eachRecord.add(String.valueOf(arrays[i][j]));
            }
            arrayList2D.add(eachRecord);
        }
        return arrayList2D;
    }

}